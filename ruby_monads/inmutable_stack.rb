# The implementation must follow this rules
# stack.push(x).top must equal x (regardless of what x is);
# stack.push(x).pop must equal stack;
# empty.empty? must be true; and
# stack.push(x).empty? must be false.
ArrayStack = Struct.new(:values) do
  def push(value)
    ArrayStack.new([value] + values)
  end

  def top
    values.first
  end

  def pop
    # build new instance based on state of current instance
    ArrayStack.new(values.drop(1))
  end

  def empty?
    values.empty?
  end

  def self.empty
    new([])
  end
end

# as = ArrayStack.empty --> instance of ArrayStack(values: [])
# as.push(9).push(4).pop()
# (ArrayStack.new([9] + [])).push(4).pop()
# ArrayStack.new([4] + self.values).pop()
# ArrayStack.new([4] + [9]).pop()
# ArrayStack.new([4, 9]).pop()
# ArrayStack.new([4, 9]).pop()
# ArrayStack.(values.drop(1))
# ArrayStack.([4, 9].drop(1))
# ArrayStack.([4])


LinkedListStack = Struct.new(:top, :pop) do
  def push(value)
    LinkedListStack.new(value, self)
  end

  # def top
  #   # we get this for free!
  # end

  # def pop
  #   # and this!
  # end

  def empty?
    pop.nil?
  end

  def self.empty
    new(nil, nil)
  end
end

# lls = LinkedListStack.empty --> an instance of LinkedListStack with top=nil, pop=nil
# ( ( lls.push(4) ).push(5) ).pop()
# ( (LinkedListStack.new(4, lls)).push(5) ).pop()
# ( LinkedListStack.new(5, (LinkedListStack.new(4, lls))).pop()
# linkedListStack.new( 2, ( LinkedListStack.new(5, (LinkedListStack.new(4, lls))))


























Project -> Person -> Address -> Country -> City(weather)
Project = Struct.new(:creator)
Person  = Struct.new(:address)
Address = Struct.new(:country)
Country = Struct.new(:capital)
City    = Struct.new(:weather)

we need to show Project and weather info

def weather_for(project)
  project.creator.address.
    country.capital.weather
end

bad_project = Project.new(Person.new(Address.new(nil)))
weather_for(bad_project)
>> NoMethodError: undefined method `capital` for nil:NilClass

we need to explicity check for them.
def weather_for(project)
  unless project.nil?
    creator = project.creator
    unless creator.nil?
      address = creator.address
      unless address.nil?
        country = address.country
        unless country.nil?
          capital = country.capital
          unless capital.nil?
            weather = capital.weather
          end
        end
      end
    end
  end
end

we can flatten this
def weather_for(project)
  unless project.nil?
    creator = project.creator
  end

  unless creator.nil?
    address = creator.address
  end

  unless address.nil?
    country = address.country
  end

  unless country.nil?
    capital = country.capital
  end

  unless capital.nil?
    weather = capital.weather
  end
end


using try
def weather_for(project)
  project.
    try(:creator).
    try(:address).
    try(:country).
    try(:capital).
    try(:weather)
end


we non-invasively add functionality to one object by wrapping it inside another object

Optional = Struct.new(:value)

instead of put try on the Object
lets add it in Optional

class Optional
  def try(*args, &block)
    if value.nil?
      nil
    else
      value.public_send(*args, &block)
    end
  end
end
def weather_for(project)
  optional_project = Optional.new(project)
  optional_creator = Optional.new(optional_project.try(:creator))
  optional_address = Optional.new(optional_creator.try(:address))
  optional_country = Optional.new(optional_address.try(:country))
  optional_capital = Optional.new(optional_country.try(:capital))
  optional_weather = Optional.new(optional_capital.try(:weather))
  weather          = optional_weather.value
end


but try does to much lets move the responsiblity to call the method
to the caller

class Optional
  def try(*args, &block)
    if value.nil?
      nil
    else
      block.call(value)
    end
  end
end
def weather_for(project)
  optional_project = Optional.new(project)
  Optional(value: project)
  optional_creator = Optional.new(optional_project.try { |project| project.creator }
  optional_address = Optional.new(optional_creator.try { |x| x.address })
  optional_country = Optional.new(optional_address.try { |x| x.country })
  optional_capital = Optional.new(optional_country.try { |x| x.capital })
  optional_weather = Optional.new(optional_capital.try { |x| x.weather })
  weather          = optional_weather.value
end


In case some of the elements returns nil
then we will have same problem again nil.capital will return an error
so we do not return nil

class Optional
  def try(&block)
    if value.nil?
      Optional.new(nil)
    else
      block.call(value)
    end
  end
end


try is a bad name for this
“start with this decorated value,
  and then do some arbitrary thing with it,
  as long as it’s not nil”.

class Optional
  def and_then(&block)
    if value.nil?
      Optional.new(nil)
    else
      block.call(value)
    end
  end
end
def weather_for(project)
  optional_project = Optional.new(project)
  optional_creator = optional_project.and_then { |project| Optional.new(project.creator) }
  optional_address = optional_creator.and_then { |creator| Optional.new(creator.address) }
  optional_country = optional_address.and_then { |address| Optional.new(address.country) }
  optional_capital = optional_country.and_then { |country| Optional.new(country.capital) }
  optional_weather = optional_capital.and_then { |capital| Optional.new(capital.weather) }
  weather          = optional_weather.value
end


Get rid of the local variables
def weather_for(project)
  Optional.new(project).
    and_then { |project| Optional.new(project.creator) }.
    and_then { |creator| Optional.new(creator.address) }.
    and_then { |address| Optional.new(address.country) }.
    and_then { |country| Optional.new(country.capital) }.
    and_then { |capital| Optional.new(capital.weather) }.
    value
end

We can make and_then to be called by default
Optional = Struct.new(:value) do
  def method_missing(*args, &block)
    if value.nil?
      Optional.new(nil)
    else
      Optional.new(value.public_send(*args, &block))
    end
  end
end


  project.creator
def weather_for(project)
  project.creator
end
